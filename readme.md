# Express CRUD - mongoDB 
## TS based express rest API

It implements a users router and handle incoming requests to **Create**, **Update** and **Delete** a user.
It also allow us to **Read** a single user and return a **list** of all existing users, with an option of **pagination**.
Our DB is  **mongoDB**.

It also implements a **log plain text file** that logs each http request made to the server in a single line and includes the request http **method**, the **path** and a **timestamp**.
In addition, it implements an **Error log plain text file** that logs each error includes the **error status**, the **error message**, the **the requestID**, a **timestamp** and the **error stack**.