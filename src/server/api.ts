import ExpressApp from "./express.app.js";

ExpressApp.setMiddlewares();
ExpressApp.setRoutings();
ExpressApp.setErrorHandlers();
ExpressApp.setDefault();
ExpressApp.start().catch(console.log);