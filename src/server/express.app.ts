import express from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";

import { connect_db } from "./db/mongoose.connection.js";
import user_router from "./modules/user/user.router.js";

import {
    errorLogger,
    not_found,
    responseWithError,
    urlNotFoundHandler,
} from "./middleware/errors.handler.js";
import { generateRequestID } from "./middleware/requestId.js";
import { appendLogToFile } from "./middleware/http.log.js";
import { appendToErrorLogger } from "./middleware/error.log.js";

const { PORT , HOST = "localhost", DB_URI } = process.env;
const { HTTP_LOG_FILE_PATH , ERRORS_LOG_FILE_PATH } = process.env;
const LEVEL = "app level";

class ExpressApp {
    app = express();

    setMiddlewares() {
        this.app.use(generateRequestID);
        this.app.use(cors());
        this.app.use(morgan("dev")); 
        this.app.use(appendLogToFile(HTTP_LOG_FILE_PATH as string,LEVEL)); 
    }

    setRoutings() {
        this.app.use("/api/users", user_router);
    }

    setErrorHandlers() {
        this.app.use(urlNotFoundHandler);
        this.app.use(errorLogger);
        this.app.use(appendToErrorLogger(ERRORS_LOG_FILE_PATH as string));
        this.app.use(responseWithError);
        // this.app.use(error_handler);
        // this.app.use(error_handler2);
    }

    setDefault() {
        this.app.use("*", not_found);
    }

    async start() {
        //connect to mongo db
        await connect_db(DB_URI as string);
        await this.app.listen(Number(PORT), HOST as string);
        log.magenta("api is live on", ` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);
    }
}

const instance = new ExpressApp();
export default instance;