import { NextFunction, Request, Response } from "express";
import fs from "fs/promises";

// Middleware for logging http requests to txt file
export const appendLogToFile = (path: string,level: string) => async (req: Request,res: Response ,next: NextFunction)=> {
    await fs.appendFile(path, `${req.method}:: log from ${level}. Request url:  ${ req.url }. Request ID: ${req.requestID}. Time: ${Math.floor(Date.now()/1000)}\n`);
    next();
};