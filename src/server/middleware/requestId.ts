import { NextFunction, Request, Response } from "express";
import { uuid } from "uuidv4";

// Middleware to generate requestID
export const generateRequestID = (req: Request, res: Response , next: NextFunction )=> {
    req.requestID = uuid();
    next();
};
