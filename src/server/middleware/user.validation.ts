import { NextFunction, Request, Response } from "express";
import {
    schemaForCreate,
    schemaForUpdate,
} from "../modules/user/user.joys.scemas.js";

// Middleware to validate user before create
export const validateBeforeCreateMW = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    await schemaForCreate.validateAsync(req.body);
    next();
};

// Middleware to validate user before update
export const validateBeforeUpdateMW = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    await schemaForUpdate.validateAsync(req.body);
    next();
};
