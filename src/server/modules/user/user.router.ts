/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";
import express, { Request, Response } from "express";
// import log from "@ajar/marker";
import {
    validateBeforeCreateMW,
    validateBeforeUpdateMW,
} from "../../middleware/user.validation.js";
import { createNewUser, getAllUsers, getUserByID, updateUser, deleteUserByID, getAllUsersWithPagination } from "./user.service.js";
import { IResponseMessage, IUser } from "../../../types/types.js";
import HttpException from "../../exceptions/http-exception.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW USER (use validation MW before)
router.post(
    "/",
    raw(validateBeforeCreateMW),
    raw(async (req: Request, res: Response) => {
        const user = await createNewUser(req.body as IUser);
        const outputResponse: IResponseMessage = {
            status: 200, 
            message: "User created",
            data: user
        };
        res.status(outputResponse.status).json(outputResponse);
    })
);

// GET ALL USERS
router.get(
    "/",
    raw(async (req: Request, res: Response) => {
        const users = await getAllUsers();
        const outputResponse: IResponseMessage = {
            status: 200, 
            message: "All existing users",
            data: users
        };
        res.status(outputResponse.status).json(outputResponse);
    })
);

// GETS A SINGLE USER
router.get(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const user = await getUserByID(req.params.id);
        if (!user){
            // return res.status(404).json({ status: "No user found." });
            throw new HttpException(400, `No such user with id: ${req.params.id}`);
        } else {
            const outputResponse: IResponseMessage = {
                status: 200, 
                message: "User found",
                data: user
            };
            res.status(outputResponse.status).json(outputResponse);
        }
    })
);
// UPDATES A SINGLE USER (use validation MW before)
router.put(
    "/:id",
    raw(validateBeforeUpdateMW),
    raw(async (req: Request, res: Response) => {
        const user = await updateUser(req.params.id,req.body);
        if (!user){
            // return res.status(404).json({ status: "No user found." });
            throw new HttpException(400, `No such user with id: ${req.params.id}`);
        } else {
            const outputResponse: IResponseMessage = {
                status: 200, 
                message: "User updated",
                data: user
            };
            res.status(outputResponse.status).json(outputResponse);
        }
    })
);

// DELETES A USER
router.delete(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const user = await deleteUserByID(req.params.id);
        if (!user) {
            // return res.status(404).json({ status: "No user found." });
            throw new HttpException(400, `No such user with id: ${req.params.id}`);
        } else {
            const outputResponse: IResponseMessage = {
                status: 200, 
                message: "User deleted",
                data: user
            };
            res.status(outputResponse.status).json(outputResponse);
        }
    })
);

// GET ALL USERS - pagination
router.get(
    "/pagination/:page/:limit",
    raw(async (req: Request, res: Response) => {
        const users = await getAllUsersWithPagination(req.params.page,req.params.limit);
        const outputResponse: IResponseMessage = {
            status: 200, 
            message: `All existing users within the page ${req.params.page} and limit ${req.params.limit}`,
            data: users
        };
        res.status(outputResponse.status).json(outputResponse);
    })
);

export default router;
