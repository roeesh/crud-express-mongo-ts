import { IUser } from "../../../types/types.js";
import user_model from "./user.model.js";


// Create new user
export async function createNewUser(reqBody: IUser): Promise<any>{
    const user = await user_model.create(reqBody);
    return user;
}

// Get all users
export async function getAllUsers(): Promise<any> {
    const users = await user_model.find().select(`-_id 
                                                first_name 
                                                last_name 
                                                email 
                                                phone`);
    return users;
}

// Get all users - with pagination
export async function getAllUsersWithPagination(page: string,limit: string): Promise<any> {
    const users = await user_model
            .find()
            .skip(Number(page) * Number(limit))
            .limit(Number(limit)).select(`-_id 
                                          first_name 
                                          last_name 
                                          email 
                                          phone`);
    return users;
}

// Get a single user
export async function getUserByID(idToRead: string): Promise<any> {
    const user = await user_model.findById(idToRead);
    return user;
}

// Update a user using the id
export async function updateUser(idToUpdate: string, reqBody: IUser): Promise<any> {
    const user = await user_model.findByIdAndUpdate(idToUpdate, reqBody,{ new: true, upsert: false });
    return user;
}

// Delete a user using the id
export async function deleteUserByID(idToDelete: string): Promise<any> {
    const user = await user_model.findByIdAndRemove(idToDelete);
    return user;
}
