import { NextFunction, Request, Response } from "express";

export interface IUser {
    id: string;
    first_name: string;
    last_name: string;
    email: string,
    phone: string
}

export interface IResponseMessage {
    status: number;
    message: string;
    data: any;
  }
export interface IErrorResponse {
    status: number;
    message: string;
    stack?: string;
}

export type usersList = IUser[];

export type middleWareFunction = (req: Request, res: Response, next: NextFunction) => any;