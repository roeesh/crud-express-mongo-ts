export declare interface IUser{
  id: string;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
}

declare global {

  namespace Express {
    interface Request {
        requestID: string;
    }
  }
}
